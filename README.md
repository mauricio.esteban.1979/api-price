# API-Price

API Restful who implements GET method getPrice for price consulting on the database

# Executable
deploy/precios-api-0.0.1.jar

#Version
0.0.1 

#Requirements
java 11


#Local Execute
java -jar precios-api-0.0.1.jar

#Endoint 
http://localhost:8090/apiPrice/getPrice

#parameters

ProductId : Id product (integer)
Brand Id : id Brand  (integer)
date : query date in String with format (yyyy-MM-dd hh:mm:ss)

example

http://localhost:8090/apiPrice/getPrice?productId=35455&brandId=1&date=2020-06-15 00:00:00

#develop stack
java 11
Springboot 2.7.13
h2 database (initial data in data.sql)

## Author
Mauricio González N.