package es.akkodis.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.akkodis.api.exception.PriceException;
import es.akkodis.api.service.ApiPreciosService;
import es.akkodis.api.to.request.PriceRequestTo;
import es.akkodis.api.to.response.PrecioResponseTo;
import es.akkodis.api.utils.Utils;

@RestController
@RequestMapping("/apiPrice")
public class ApiPreciosController {

	@Autowired
	private ApiPreciosService service;


	@GetMapping("/getPrice")
	public PrecioResponseTo obtienePrecio(@RequestParam(name = "productId") int productId, 
			@RequestParam(name = "brandId") int brandId,@RequestParam(name = "date") String date) {
							 
		PrecioResponseTo response = new PrecioResponseTo();

		try {
			PriceRequestTo request;
			request = Utils.validateRequest(productId, brandId, date);
			response = service.getPrice(request);

		} catch (PriceException e) {
			response.setException(e.getMessage());
		}

		return response;

	}

}
