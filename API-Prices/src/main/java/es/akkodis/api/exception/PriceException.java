package es.akkodis.api.exception;

public class PriceException extends Exception{

	private static final long serialVersionUID = 1L;
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private String message;

	public PriceException(Exception e) {
		super(e);
	}

	public PriceException(String message) {
	 this.message = message;
	}
	
 

}
