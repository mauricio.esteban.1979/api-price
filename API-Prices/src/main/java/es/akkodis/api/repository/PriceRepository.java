package es.akkodis.api.repository;

 
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import es.akkodis.api.model.PriceEntity;


@Repository
public interface PriceRepository extends JpaRepository<PriceEntity, Long>{
	 
	@Query("SELECT p FROM PriceEntity p where p.brandId.id=?1 and p.productId.id=?2 and ?3 between p.startDate and p.endDate order by p.priority desc")//p.startDate>=?3 and
	List<PriceEntity> searchWithParams(Long brand_id, Long product_id, Date date);
	
	 

}
