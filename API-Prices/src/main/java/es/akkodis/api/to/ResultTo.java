package es.akkodis.api.to;

public class ResultTo {
	
	private Long id;
	private Long product_id;
	
	public ResultTo(){

}
	
	public ResultTo(Long id, Long product_id){
		this.id = id;
		this.product_id=product_id;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}
	
}
