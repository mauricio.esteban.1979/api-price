package es.akkodis.api.service;

import es.akkodis.api.exception.PriceException;
import es.akkodis.api.to.request.PriceRequestTo;
import es.akkodis.api.to.response.PrecioResponseTo;

public interface ApiPreciosService {

	PrecioResponseTo getPrice(PriceRequestTo request) throws PriceException;

}
