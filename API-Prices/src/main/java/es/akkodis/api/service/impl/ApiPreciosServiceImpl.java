package es.akkodis.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.akkodis.api.exception.PriceException;
import es.akkodis.api.model.PriceEntity;
import es.akkodis.api.repository.PriceRepository;
import es.akkodis.api.service.ApiPreciosService;
import es.akkodis.api.to.request.PriceRequestTo;
import es.akkodis.api.to.response.PrecioResponseTo;
import es.akkodis.api.utils.Utils;

@Service
public class ApiPreciosServiceImpl implements ApiPreciosService {

	private static final String NOT_FOUND = "Not found";
	private static final String ERROR = "Error ";

	@Autowired
	private PriceRepository repo;

	@Override
	public PrecioResponseTo getPrice(PriceRequestTo request) throws PriceException {

		PrecioResponseTo response = new PrecioResponseTo();
		try {

			Optional<List<PriceEntity>> result = Optional
					.ofNullable(repo.searchWithParams(request.getBrandId(), request.getProductId(), request.getDate()));

			if (result.isPresent()) {
				response = Utils.convert(result.get().get(0));
			} else {
				throw new PriceException(NOT_FOUND);
			}

		} catch (Exception e) {
			throw new PriceException(ERROR + " " + e.getMessage());
		}

		return response;
	}

}
