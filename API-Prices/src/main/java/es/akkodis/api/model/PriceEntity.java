package es.akkodis.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "price")
public class PriceEntity implements Serializable {
	/*
	 * BRAND_ID: foreign key de la cadena del grupo (1 = ZARA). START_DATE ,
	 * END_DATE: rango de fechas en el que aplica el	 precio tarifa indicado.
	 * PRICE_LIST: Identificador de la tarifa de precios aplicable. PRODUCT_ID:
	 * Identificador código de producto. PRIORITY: Desambiguador de aplicación de
	 * precios. Si dos tarifas coinciden en un rago de fechas se aplica la de mayor
	 * prioridad (mayor valor numérico). PRICE: precio final de venta. CURR: iso de
	 * la moneda.
	 */

	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "brand_id", referencedColumnName = "id")
	private BrandEntity brandId;	
	 

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private ProductEntity productId;

	@Column(name = "start_date")
	@DateTimeFormat(pattern="yyyy-MM-dd-HH.mm.ss")
	private Date  startDate;
	
	@Column(name = "end_date")
	@DateTimeFormat(pattern="yyyy-MM-dd-HH.mm.ss")
	private Date  endDate;

	@Column(name = "price_list")
	private Long priceList;
 
	@Column(name = "priority")
	private Long priority;	
 
	@Column(name = "price") 	
	private BigDecimal price;

	@Column(name = "curr")
	private String curr;
	
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
 
	public ProductEntity getProductId() {
		return productId;
	}

	public void setProductId(ProductEntity productId) {
		this.productId = productId;
	}
	 
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getPriceList() {
		return priceList;
	}

	public void setPriceList(Long priceList) {
		this.priceList = priceList;
	}

	public Long getPriority() {
		return priority;
	}

	public void setPriority(Long priority) {
		this.priority = priority;
	}

	public String getCurr() {
		return curr;
	}

	public void setCurr(String curr) {
		this.curr = curr;
	}

	public BrandEntity getBrandId() {
		return brandId;
	}

	public void setBrandId(BrandEntity brandId) {
		this.brandId = brandId;
	}
	public PriceEntity() {
	}

}
