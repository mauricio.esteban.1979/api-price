package es.akkodis.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name ="product")
@Entity
public class ProductEntity implements Serializable {
	
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Id
	private Long id;
	
	 @Column(name ="name")
	private String name;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
