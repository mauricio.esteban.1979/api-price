package es.akkodis.api.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import es.akkodis.api.exception.PriceException;
import es.akkodis.api.model.PriceEntity;
import es.akkodis.api.to.request.PriceRequestTo;
import es.akkodis.api.to.response.PrecioResponseTo;

public class Utils {
	
	private static final String PARSE_FORMAT_ERROR = "Format error";
	

	/** Convierte resultado entity a to respuesta api */
	public static PrecioResponseTo convert(PriceEntity price) {
		PrecioResponseTo response = new PrecioResponseTo();
		response.setBrandId(price.getBrandId().getId());
		response.setEndDate(price.getEndDate());
		response.setStartDate(price.getStartDate());
		response.setPrice(price.getPrice());
		response.setProductId(price.getProductId().getId());
		response.setPriceList(price.getPriceList());
		return response;
	}
	
	/** Valida request */
	
	public static PriceRequestTo validateRequest(int productId, int brandId, String date) 
			throws PriceException{
		PriceRequestTo req = new PriceRequestTo();
		Date fecha = new Date();
		try {
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			fecha = formato.parse(date);
			Long.valueOf(brandId);
			Long.valueOf(productId);	
			req.setDate(fecha);
			req.setBrandId((long)brandId);
			req.setProductId((long)productId);
	 
			
		}		
		catch (Exception e) {		
			throw new PriceException(PARSE_FORMAT_ERROR +" productId:"+productId+" brandId:"+brandId+ " date:"+date);
		}
		return req;
	}

}
