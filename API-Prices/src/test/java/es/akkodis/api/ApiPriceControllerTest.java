package es.akkodis.api;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import es.akkodis.api.controller.ApiPreciosController;
import es.akkodis.api.to.response.PrecioResponseTo;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ApiPriceControllerTest {
	
	private static final String UNO_TEST_FECHA = "2020-06-14 10:00:01";
	private static final String DOS_TEST_FECHA = "2020-06-14 16:00:01";
	private static final String TRES_TEST_FECHA = "2020-06-14 21:00:01";
	private static final String CUATRO_TEST_FECHA = "2020-06-15 10:00:01";
	private static final String CINCO_TEST_FECHA = "2020-06-16 21:00:01";
	private static final String TEST_FECHA_NOK = "20202323-06-15 10:00:01";

	@InjectMocks 	
	private ApiPreciosController service;
	
 	@Autowired 
	private ApiPreciosController controller;
	

	
	@Test
	public void testPriceNotNull() throws Exception 
	{
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));       
		PrecioResponseTo responseEntity = controller.obtienePrecio(35455,1, UNO_TEST_FECHA);
        assertThat(responseEntity.getException()).isNull();
	}
	
	@Test
	public void testPriceNull() throws Exception 
	{
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        PrecioResponseTo responseEntity = controller.obtienePrecio(35455,1, TEST_FECHA_NOK); 
        assertThat(responseEntity.getException()).isNotNull();
	}
	 
	
	@Test
	public void testPrice1() throws Exception 
	{
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));       
		PrecioResponseTo responseEntity = controller.obtienePrecio(35455,1, UNO_TEST_FECHA);
        assertThat(responseEntity.getPrice()).isEqualTo(new BigDecimal("35.50"));
	}
	
	@Test
	public void testPrice2() throws Exception 
	{
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));       
		PrecioResponseTo responseEntity = controller.obtienePrecio(35455,1, DOS_TEST_FECHA);
        assertThat(responseEntity.getPrice()).isEqualTo(new BigDecimal("35.50"));
	}
	
	@Test
	public void testPrice3() throws Exception 
	{
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));       
		PrecioResponseTo responseEntity = controller.obtienePrecio(35455,1, TRES_TEST_FECHA);
        assertThat(responseEntity.getPrice()).isEqualTo(new BigDecimal("35.50"));
	}
	
	@Test
	public void testPrice4() throws Exception 
	{
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));       
		PrecioResponseTo responseEntity = controller.obtienePrecio(35455,1, CUATRO_TEST_FECHA);
        assertThat(responseEntity.getPrice()).isEqualTo(new BigDecimal("30.50"));
	}
	
	@Test
	public void testPrice5() throws Exception 
	{
		MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));       
		PrecioResponseTo responseEntity = controller.obtienePrecio(35455,1, CINCO_TEST_FECHA);
        assertThat(responseEntity.getPrice()).isEqualTo(new BigDecimal("25.45"));
	}
	

}
