package es.akkodis.api;

 
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import es.akkodis.api.model.PriceEntity;
import es.akkodis.api.repository.PriceRepository;
	
@SpringBootTest
public class ApiPriceRepositoryTest {

	@Autowired
    private PriceRepository repository;

    @Test
    void testSave() {
        PriceEntity price = repository.save(new PriceEntity());        
        Assertions.assertNotNull(price);
      
    }
    
}